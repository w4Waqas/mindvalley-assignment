package com.mindvalley.newtorklib.listeners;

/**
 * Created by muhammadw-c on 1/17/17.
 */

public interface VolleyErrorListener {
    void onErrorResponse(String error, int errorCode);
}
