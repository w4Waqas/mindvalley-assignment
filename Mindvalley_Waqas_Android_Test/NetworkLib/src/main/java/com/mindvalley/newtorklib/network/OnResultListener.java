package com.mindvalley.newtorklib.network;

/**
 * Created by muhammadw-c on 1/17/17.
 */

public interface OnResultListener {
    void onResult(Result result, int errorCode, String errorString);
}