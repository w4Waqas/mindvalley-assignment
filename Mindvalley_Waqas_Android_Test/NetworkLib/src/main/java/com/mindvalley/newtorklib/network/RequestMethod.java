package com.mindvalley.newtorklib.network;

public enum RequestMethod
{
GET,
POST,
MULTIPART_FORM,
PUT
}
