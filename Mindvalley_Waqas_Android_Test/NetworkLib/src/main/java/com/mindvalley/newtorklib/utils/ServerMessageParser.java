package com.mindvalley.newtorklib.utils;

import android.util.Log;

import org.json.JSONObject;

public class ServerMessageParser {

    public static String pasre(Object response) {
        JSONObject jsonobject = null;
        String message = null;
        if (response != null) {
            try {
                jsonobject = new JSONObject(response.toString());
            } catch (Exception e) {
                Log.d("ServerMessageParser", e.getMessage());
            }
            if (jsonobject != null) {
                message = jsonobject.optString("Message");
            }
        }

        return message;
    }
}