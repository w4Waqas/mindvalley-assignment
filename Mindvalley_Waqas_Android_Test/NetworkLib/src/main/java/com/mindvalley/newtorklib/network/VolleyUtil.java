package com.mindvalley.newtorklib.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.util.LruCache;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mindvalley.newtorklib.listeners.VolleyErrorListener;
import com.mindvalley.newtorklib.listeners.VolleySuccessListener;
import com.mindvalley.newtorklib.utils.JsonUtil;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class VolleyUtil {
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String TAG = "Volley";
    private static final int MY_SOCKET_TIMEOUT_MS = 120000;

    private static RequestQueue requestQueue;
    private static ImageLoader mImageLoader;

    public static StringRequest sendVolleyGetRequest(Context context, final String url, ResponseType type, final VolleySuccessListener successListener, final VolleyErrorListener errorListener) {
        RequestQueue requestQueue = getRequestQueue(context);
        Response.ErrorListener volleyErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (errorListener != null) {
                    int status  = -1;

                    if(volleyError.networkResponse != null)
                    {
                        status  = volleyError.networkResponse.statusCode;

                        Log.e(TAG + "Error", volleyError.toString());
                    }
                    errorListener.onErrorResponse(handleVolleyError(volleyError), status);
                }
            }
        };
        Response.Listener volleySuccessListener = new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                try {
                    successListener.onSuccessResponse(response.toString());
                } catch (Exception e) {
                    Log.d(TAG, e.toString());
                    successListener.onSuccessResponse(response.toString());
                }
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, volleySuccessListener, volleyErrorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = new HashMap<>();
               /*
                * Put Authorization header here

                if(Header available) {
                    headerMap.put(HEADER_AUTHORIZATION, "Put Authorization header here");
                }
                 */
                return headerMap;
            }
        };
        requestQueue.add(stringRequest);
        return stringRequest;
    }


    public static StringRequest sendVolleyRequestWithBody(Context context, int method, String url, String body, final VolleySuccessListener successListener, final VolleyErrorListener errorListener) {
        RequestQueue requestQueue = getRequestQueue(context);
        Response.ErrorListener volleyErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (errorListener != null) {
                    int status  = -1;

                    if(volleyError.networkResponse != null)
                    {
                        status  = volleyError.networkResponse.statusCode;

                        Log.e(TAG + "Error", volleyError.toString());
                    }
                    errorListener.onErrorResponse(handleVolleyError(volleyError), status);
                }
            }
        };
        Response.Listener volleySuccessListener = new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                try {
                    successListener.onSuccessResponse(response.toString());
                } catch (Exception e) {
                    Log.d(TAG, e.toString());
                    successListener.onSuccessResponse(response.toString());
                }
            }
        };

        CustomBodyStringRequest request = new CustomBodyStringRequest(url, method,  body,  volleySuccessListener, volleyErrorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = new HashMap<>();

                /*
                * Put Authorization header here

                if(Header available) {
                    headerMap.put(HEADER_AUTHORIZATION, "Put Authorization header here");
                }
                 */

                return headerMap;
            }
        };

        requestQueue.add(request);
        return request;
    }


    private static RequestQueue getRequestQueue(Context context) {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }
        return requestQueue;
    }

    public static ImageLoader getImageLoader(Context context) {
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(getRequestQueue(context),
                    new ImageLoader.ImageCache() {
                        private final LruCache<String, Bitmap>
                                cache = new LruCache<String, Bitmap>(20);

                        @Override
                        public Bitmap getBitmap(String url) {
                            return cache.get(url);
                        }

                        @Override
                        public void putBitmap(String url, Bitmap bitmap) {
                            cache.put(url, bitmap);
                        }
                    });
        }
        return mImageLoader;
    }

    private static String handleVolleyError(VolleyError error) {
        String errorStr = WebServiceErrors.ERROR_SERVICE_SENDING_REQUEST;
        if (error.networkResponse != null) {
            try {
                errorStr = getResponseString(error.networkResponse.statusCode);
            } catch (Exception e) {
                Log.d(TAG, e.toString());
                errorStr = getResponseString(error.networkResponse.statusCode);
            }

        }
        return errorStr;
    }

    public static String getResponseString(int responseCode) {
        if (responseCode == 403) {
            return "Invalid Email or Password";
        }
        int resCodePrefix = responseCode / 100;
        if (resCodePrefix == 5) {
            return WebServiceErrors.RESPONSE_SERVER_SIDE.replace("??", responseCode + "");
        } else {
            return WebServiceErrors.RESPONSE_CLIENT_SIDE.replace("??", responseCode + "");
        }
    }

    public static boolean isConnectedToNetwork(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
