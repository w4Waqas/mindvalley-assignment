package com.mindvalley.newtorklib.utils;


/**
 * StringUtil.java
 */

public abstract class StringUtil {

    private static String TAG = "StringUtil";

    /*
     * Checks whether the Value String is Empty or Not
     */
    public static boolean isEmpty(String value) {
        if (value == null || "".equals(value.trim())) {
            return true;
        }
        return false;
    }

}
