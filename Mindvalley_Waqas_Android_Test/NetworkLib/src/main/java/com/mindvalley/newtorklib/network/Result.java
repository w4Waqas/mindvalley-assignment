package com.mindvalley.newtorklib.network;

import java.util.ArrayList;

/**
 * Created by muhammadw-c on 1/17/17.
 */

public class Result {

    private ArrayList<Object> mResultList;
    private Object mResultObj;

    public Result()
    {

    }

    public Result(ArrayList<Object> mResultList) {
        super();
        this.mResultList = mResultList;
    }

    public Result(Object object) {
        super();
        this.mResultObj = object;
    }

    public ArrayList<Object> getResultList() {
        return mResultList;
    }

    public void setResultList(ArrayList<Object> mResultList) {
        this.mResultList = mResultList;
    }

    public Object getResultObj() {
        return mResultObj;
    }

    public void setResultObj(Object obj) {
        this.mResultObj = obj;
    }
}

