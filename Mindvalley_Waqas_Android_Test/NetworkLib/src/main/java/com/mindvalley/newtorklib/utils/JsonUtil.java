package com.mindvalley.newtorklib.utils;

import android.util.Log;

import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.List;

public class JsonUtil {

	private static String TAG = "JsonUtil";

	private static Gson INSTANCE = null;

	public static Gson getInstance() {

		if (INSTANCE == null) {
			INSTANCE = new Gson();
		}

		return INSTANCE;
	}

	public static String toJson(Object object) {
		String jsonString = "";

		try {
			jsonString = getInstance().toJson(object);
		} catch (Exception e) {
			Log.d(TAG, e.toString());
		}

		return jsonString;
	}

	public static <T> Object fromJson(String jsonString, Class<T> clazz) {
		T object = null;
		try {
			object = getInstance().fromJson(jsonString, clazz);
		} catch (Exception e) {
			Log.d(TAG, e.toString());
		}

		return object;
	}

	public static <T> List<T> fromJsonList(String jsonString, Type listType){

		List<T> lst = null;
		try {
			lst = getInstance().fromJson(jsonString, listType);
		} catch (Exception e) {
			Log.d(TAG, e.toString());
		}

		return lst;				 
	}

	
	public static boolean isValidJSON(String jsonString){
		if(!StringUtil.isEmpty(jsonString)){
			if(jsonString.startsWith("[")|| jsonString.startsWith("{")){
				return true;
			}
		}
		return false;
	}
	
	
}
