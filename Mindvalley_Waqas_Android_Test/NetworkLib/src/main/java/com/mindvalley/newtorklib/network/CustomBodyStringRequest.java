package com.mindvalley.newtorklib.network;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.mindvalley.newtorklib.utils.StringUtil;

import java.io.UnsupportedEncodingException;

public class CustomBodyStringRequest extends StringRequest {
    private final String requestBody;

    public CustomBodyStringRequest(String url, int method, String requestBody, Response.Listener<String> listener,
                                   Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.requestBody = requestBody;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        byte[] body = null;
        if (!StringUtil.isEmpty(this.requestBody)) {
            try {
                body = requestBody.getBytes(getParamsEncoding());
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("Encoding not supported: " + getParamsEncoding(), e);
            }
        }

        return body;
    }
}