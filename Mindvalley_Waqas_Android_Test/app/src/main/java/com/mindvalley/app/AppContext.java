package com.mindvalley.app;

import android.app.Application;
import android.content.Context;

import com.mindvalley.network.LiveDataProvider;
import com.mindvalley.newtorklib.ImageLoader.ImageDownloader;


/**
 * Created by muhammadw-c on 1/17/17.
 */

public class AppContext extends Application {

    public static Context context;
    public static LiveDataProvider dataProvider;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        // Initialize Live Data Provider to interact with network layer
        dataProvider = new LiveDataProvider(this) ;

        //  Initialize Image downloader to download images from server and give the image directory name.
        ImageDownloader.initImageLoader(this, "MyDir");
    }

}
