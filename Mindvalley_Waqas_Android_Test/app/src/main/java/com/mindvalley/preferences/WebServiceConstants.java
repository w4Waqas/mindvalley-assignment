package com.mindvalley.preferences;

/**
 * Created by muhammadw-c on 1/17/17.
 */

public class WebServiceConstants {

    public static final String DomainUrl = "http://pastebin.com/";
    public static final String BASE_URL = DomainUrl + "raw/";
    public static final String ImageListUrl = BASE_URL + "wgkJgazE";
    
}
