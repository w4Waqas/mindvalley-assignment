package com.mindvalley.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import com.android.volley.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mindvalley.app.AppContext;
import com.mindvalley.models.Image;
import com.mindvalley.models.User;
import com.mindvalley.newtorklib.listeners.VolleyErrorListener;
import com.mindvalley.newtorklib.listeners.VolleySuccessListener;
import com.mindvalley.newtorklib.network.OnResultListener;
import com.mindvalley.newtorklib.network.ResponseType;
import com.mindvalley.newtorklib.network.Result;
import com.mindvalley.newtorklib.network.VolleyUtil;
import com.mindvalley.newtorklib.utils.JsonUtil;

import org.json.JSONObject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by muhammadw-c on 1/17/17.
 */

public class LiveDataProvider {ProgressDialog mProgressDialog;

    JSONObject mJSONEventObj;
    public static AppContext appContext;
    private static String TAG = "LiveDataProvider";

    public LiveDataProvider(AppContext app) {
        appContext = app;
    }

    public  void getImageList(Context context, String url, ResponseType type,
                                 final OnResultListener listener){
        VolleySuccessListener successListener = new VolleySuccessListener() {
            @Override
            public void onSuccessResponse(Object response) {
                try{
                    if(response!=null && JsonUtil.isValidJSON(response.toString())) {
                        Gson gson = JsonUtil.getInstance();
                        Type listType = new TypeToken<List<Image>>(){}.getType();
                        ArrayList<Image> imageList = (ArrayList<Image>) gson.fromJson(response.toString(), listType);
                        listener.onResult(new Result(imageList), 0, null);

                    } else{
                        listener.onResult(null, 0, null);
                    }

                }catch (Exception e){
                    Log.d(TAG, e.toString());
                }
            }
        };

        VolleyErrorListener errorListener = new VolleyErrorListener() {
            @Override
            public void onErrorResponse(String error, int errorCode) {
                listener.onResult(null, errorCode, error);
            }
        };
        VolleyUtil.sendVolleyGetRequest(context, url, type, successListener, errorListener);
    }


}

