package com.mindvalley.utils;


import com.mindvalley.models.ImageCategory;

import java.util.List;


/**
 * StringUtil.java
 */

public abstract class StringUtil {

    public static String joinCategories(List<ImageCategory> list) {

        String str = "";
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                if (i == 0) {
                    str = list.get(i).getTitle();
                } else {
                    str = str + ", " + list.get(i).getTitle();
                }
            }
        }
        return str;
    }
}
