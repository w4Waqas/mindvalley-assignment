package com.mindvalley.models;

/**
 * Created by muhammadw-c on 1/17/17.
 */

public class ImageCategory {
    private String id;

    private String title;

    private Links links;

    private String photo_count;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public Links getLinks ()
    {
        return links;
    }

    public void setLinks (Links links)
    {
        this.links = links;
    }

    public String getPhoto_count ()
    {
        return photo_count;
    }

    public void setPhoto_count (String photo_count)
    {
        this.photo_count = photo_count;
    }

}
