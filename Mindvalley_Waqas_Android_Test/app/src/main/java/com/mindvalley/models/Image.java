package com.mindvalley.models;

import java.util.List;

/**
 * Created by muhammadw-c on 1/17/17.
 */

public class Image {

    private String id;

    private String height;

    private CurrenrUserCollection[] current_user_collections;

    private String color;

    private ImageUrls urls;

    private String likes;

    private String width;

    private String created_at;

    private Links links;

    private List<ImageCategory> categories;

    private User user;

    private boolean liked_by_user;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getHeight ()
    {
        return height;
    }

    public void setHeight (String height)
    {
        this.height = height;
    }

    public CurrenrUserCollection[] getCurrent_user_collections ()
    {
        return current_user_collections;
    }

    public void setCurrent_user_collections (CurrenrUserCollection[] current_user_collections)
    {
        this.current_user_collections = current_user_collections;
    }

    public String getColor ()
    {
        return color;
    }

    public void setColor (String color)
    {
        this.color = color;
    }

    public ImageUrls getUrls ()
    {
        return urls;
    }

    public void setUrls (ImageUrls urls)
    {
        this.urls = urls;
    }

    public String getLikes ()
    {
        return likes;
    }

    public void setLikes (String likes)
    {
        this.likes = likes;
    }

    public String getWidth ()
    {
        return width;
    }

    public void setWidth (String width)
    {
        this.width = width;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public Links getLinks ()
    {
        return links;
    }

    public void setLinks (Links links)
    {
        this.links = links;
    }

    public List<ImageCategory> getCategories ()
    {
        return categories;
    }

    public void setCategories (List<ImageCategory> categories)
    {
        this.categories = categories;
    }

    public User getUser ()
    {
        return user;
    }

    public void setUser (User user)
    {
        this.user = user;
    }

    public boolean getLiked_by_user ()
    {
        return liked_by_user;
    }

    public void setLiked_by_user (boolean liked_by_user)
    {
        this.liked_by_user = liked_by_user;
    }

}
