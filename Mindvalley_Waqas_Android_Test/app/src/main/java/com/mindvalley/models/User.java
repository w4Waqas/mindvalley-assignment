package com.mindvalley.models;

/**
 * Created by muhammadw-c on 1/17/17.
 */

public class User {

    private String id;

    private String username;

    private UserProfileImageUrls profile_image;

    private String name;

    private Links links;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public UserProfileImageUrls getProfile_image ()
    {
        return profile_image;
    }

    public void setProfile_image (UserProfileImageUrls profile_image)
    {
        this.profile_image = profile_image;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Links getLinks ()
    {
        return links;
    }

    public void setLinks (Links links)
    {
        this.links = links;
    }
}
