package com.mindvalley.models;

/**
 * Created by muhammadw-c on 1/17/17.
 */

public class UserProfileImageUrls {
    private String small;

    private String large;

    private String medium;

    public String getSmall ()
    {
        return small;
    }

    public void setSmall (String small)
    {
        this.small = small;
    }

    public String getLarge ()
    {
        return large;
    }

    public void setLarge (String large)
    {
        this.large = large;
    }

    public String getMedium ()
    {
        return medium;
    }

    public void setMedium (String medium)
    {
        this.medium = medium;
    }
}
