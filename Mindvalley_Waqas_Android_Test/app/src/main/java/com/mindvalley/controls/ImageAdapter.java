package com.mindvalley.controls;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mindvalley.R;
import com.mindvalley.app.AppContext;
import com.mindvalley.models.Image;
import com.mindvalley.newtorklib.ImageLoader.ImageDownloader;
import com.mindvalley.utils.StringUtil;

import java.util.ArrayList;


/**
 * Created by muhammadw-c on 1/17/17.
 */

public class ImageAdapter extends ArrayAdapter<Image> {
    private Context context;
    private ArrayList<Image> imageList;
    private static String TAG = "ImageAdapter";

    static class ViewHolder {

        public TextView name;
        public TextView category;
        public ImageView image;
        public Image selectedImage;
    }

    public ImageAdapter(Context context, ArrayList<Image> imageList) {
        super(context, R.layout.activity_main_nav, imageList);
        try {
            this.context = context;
            this.imageList = imageList;


        }catch (Exception e){
            Log.d(TAG, e.toString());
        }
    }


    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public Image getItem(int position) {
        return imageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.image_list_item, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView)rowView.findViewById(R.id.txtName);
            viewHolder.category = (TextView)rowView.findViewById(R.id.txtCategory);
            viewHolder.image = (ImageView)rowView.findViewById(R.id.image);

            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder viewHolder = (ViewHolder) rowView.getTag();
        final Image img = imageList.get(position);
        if(img!=null) {
            viewHolder.name.setText(img.getUser().getName());
            viewHolder.category.setText(StringUtil.joinCategories(img.getCategories()));
            if(img.getUrls()!=null && img.getUrls().getThumb()!=null){
//                AppContext.imageLoader.displayImage(img.getUrls().getThumb(), viewHolder.image, AppContext.getDisplayImageOptions());
                ImageDownloader.imageLoader.displayImage(img.getUrls().getThumb(), viewHolder.image, ImageDownloader.getDisplayImageOptions());
            }

        }

       //rowView.setTag(img);
        return rowView;
    }

}
