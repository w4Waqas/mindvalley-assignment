package com.mindvalley.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.mindvalley.R;
import com.mindvalley.activities.MainNavigationActivity;
import com.mindvalley.app.AppContext;
import com.mindvalley.controls.ImageAdapter;
import com.mindvalley.models.Image;
import com.mindvalley.newtorklib.network.OnResultListener;
import com.mindvalley.newtorklib.network.ResponseType;
import com.mindvalley.newtorklib.network.Result;
import com.mindvalley.preferences.WebServiceConstants;

import java.util.ArrayList;

/**
 * Created by muhammadw-c on 1/17/17.
 */

public class ImageListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    //private ProgressDialog progressDialog;
    ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public ImageAdapter adapter;
    ArrayList<Image> imageList;
    View v;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        this.v = inflater.inflate(R.layout.image_list_fragment, container, false);


        initViews();

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        FetchImageListFromServer();
                                    }
                                }
        );


        return v;
    }

    public void initViews(){

        context = getActivity();
        listView = (ListView) v.findViewById(R.id.imageList);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    public void FetchImageListFromServer(){
       // progressDialog = ProgressDialog.show(this.getActivity(), getString(R.string.app_name), getString(R.string.please_wait), false);
        // showing refresh animation before making http call
        swipeRefreshLayout.setRefreshing(true);

        AppContext.dataProvider.getImageList(getActivity(), WebServiceConstants.ImageListUrl, ResponseType.Json, new OnResultListener() {
            @Override
            public void onResult(Result result, int errorCode, String errorString) {
                //progressDialog.hide();
                // stopping swipe refresh
                swipeRefreshLayout.setRefreshing(false);

                if (result != null) {
                    imageList = (ArrayList<Image>) result.getResultObj();
                    populateData(imageList);
                } else if (errorString != null) {
                    try {
                        Toast.makeText(getActivity(), errorString, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                    }
                } else {
                    Toast.makeText(getActivity(), R.string.msg_error_server_connection, Toast.LENGTH_LONG).show();
                }
            }
        });
    }



    public void populateData(final ArrayList<Image> imageList) {
        if (imageList != null) {
        adapter = new ImageAdapter(getActivity(), imageList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Image image = (Image) view.getTag();
               ((MainNavigationActivity)context).loadDetails(imageList.get(i));
            }
        });
        }
    }



    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        ((MainNavigationActivity)getActivity()).showBackbutton(false);
        super.onResume();

    }

    @Override
    public void onRefresh() {
        FetchImageListFromServer();
    }
}
