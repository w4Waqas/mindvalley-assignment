package com.mindvalley.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;


import com.mindvalley.R;
import com.mindvalley.activities.MainNavigationActivity;
import com.mindvalley.models.Image;
import com.mindvalley.newtorklib.ImageLoader.ImageDownloader;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by muhammadw-c on 1/23/17.
 */

public class ImageDetailFragment extends Fragment {

    private ProgressDialog progressDialog;
    View v;
    Context context;
    GridView gridView;
    CircleImageView userAvatar;
    Image image;
    TextView tvUserName;


    public void setSelectedImage(Image selectedImage){
        image = selectedImage;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.v = inflater.inflate(R.layout.image_detail_fragment, container, false);


        initViews();
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return v;
    }

    public void initViews(){

        context = getActivity();
        //gridView = (GridView) v.findViewById(R.id.gridView);
        userAvatar = (CircleImageView)v.findViewById(R.id.imgUser);
        tvUserName = (TextView)v.findViewById(R.id.tvUserName);
        tvUserName.setText(image.getUser().getName());

        if(image.getUrls()!=null && image.getUrls().getThumb()!=null){
            ImageDownloader.imageLoader.displayImage(image.getUrls().getThumb(), userAvatar, ImageDownloader.getDisplayImageOptions());
        }

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        ((MainNavigationActivity)getActivity()).showBackbutton(true);
        super.onResume();

    }

}
